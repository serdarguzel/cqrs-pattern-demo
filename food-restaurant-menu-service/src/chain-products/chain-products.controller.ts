import { Controller, Get, Param, Render } from '@nestjs/common';
import { ChainProductsService } from './chain-products.service';

@Controller('chain-products')
export class ChainProductsController {
  constructor(private readonly chainProductsService: ChainProductsService) {}

  @Get('')
  getChainProductsCtrl() {
    return this.chainProductsService.getChainProductsService();
  }

  @Get(':chainProductId')
  getChainProductCtrl(@Param('chainProductId') chainProductId) {
    return this.chainProductsService.getChainProductByIdService(chainProductId);
  }
}
