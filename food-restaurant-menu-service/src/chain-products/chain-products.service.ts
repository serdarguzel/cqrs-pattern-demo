import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ChainProductsEntity } from './chain-products.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class ChainProductsService {
  constructor(
    @InjectRepository(ChainProductsEntity)
    private readonly chainProductsEntityRepository: MongoRepository<ChainProductsEntity>,
  ) {}

  async getChainProductsService() {
    return await this.chainProductsEntityRepository.find();
  }

  async getChainProductByIdService(chainProductId) {
    return await this.chainProductsEntityRepository.findOne({ chainProductId });
  }
}
