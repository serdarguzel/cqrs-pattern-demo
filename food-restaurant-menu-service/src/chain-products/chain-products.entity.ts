import {
  Entity,
  Column,
  ObjectIdColumn,
  ObjectID,
} from 'typeorm';

@Entity('chainproducts')
export class ChainProductsEntity {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  chainProductId: string;

  @Column()
  name: string;

  @Column()
  chainRestaurant: string;

  @Column()
  status: string;

  constructor(chainProduct?: Partial<ChainProductsEntity>) {
    Object.assign(this, chainProduct);
  }
}
