import { Controller, Get, Param, Render } from '@nestjs/common';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get('')
  getProductsCtrl() {
    return this.productsService.getProductsService();
  }

  @Get(':productId')
  getProductCtrl(@Param('productId') productId) {
    return this.productsService.getProductByIdService(productId);
  }
}
