import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductsEntity } from './products.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductsEntity)
    private readonly productsEntityRepository: MongoRepository<ProductsEntity>,
  ) {}

  async getProductsService() {
    return await this.productsEntityRepository.find();
  }

  async getProductByIdService(productId) {
    return await this.productsEntityRepository.findOne({ productId });
  }
}
