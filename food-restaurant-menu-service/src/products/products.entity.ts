import {
  Entity,
  Column,
  ObjectIdColumn,
  CreateDateColumn,
  ObjectID,
} from 'typeorm';

@Entity('products')
export class ProductsEntity {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  productId: string;

  @Column()
  name: string;

  @Column()
  chainRestaurant: string;

  @Column()
  restaurant: string;

  @Column()
  status: string;

  constructor(product?: Partial<ProductsEntity>) {
    Object.assign(this, product);
  }
}
