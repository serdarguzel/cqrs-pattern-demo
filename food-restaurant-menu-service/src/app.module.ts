import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChainProductsEntity } from './chain-products/chain-products.entity';
import { ChainProductsService } from './chain-products/chain-products.service';
import { ChainProductsController } from './chain-products/chain-products.controller';
import { ProductsEntity } from './products/products.entity';
import { ProductsController } from './products/products.controller';
import { ProductsService } from './products/products.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb://localhost:27017/db',
      database: 'db',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      ssl: false,
      useUnifiedTopology: true,
      useNewUrlParser: true,
    }),
    TypeOrmModule.forFeature([ChainProductsEntity, ProductsEntity]),
  ],
  controllers: [AppController, ChainProductsController, ProductsController],
  providers: [AppService, ChainProductsService, ProductsService],
})
export class AppModule {}
