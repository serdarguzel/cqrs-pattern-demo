# Generated by Django 3.2.6 on 2021-08-03 13:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='chainoptioncategory',
            name='chain_restaurant',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.chainrestaurant'),
        ),
        migrations.AddField(
            model_name='chainproduct',
            name='chain_restaurant',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.chainrestaurant'),
        ),
        migrations.AddField(
            model_name='optioncategory',
            name='restaurant',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.restaurant'),
        ),
        migrations.AddField(
            model_name='product',
            name='restaurant',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.restaurant'),
        ),
    ]
