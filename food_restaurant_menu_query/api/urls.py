from django.urls import path
from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()


urlpatterns = [
	path('generate_fake_data/', GenerateFakeDataView.as_view(), name='generate_fake_data'),
	path('update_chain_product_status/', UpdateChainProductStatusView.as_view(), name='update_chain_product_status'),
]

urlpatterns += router.urls

"""
from django_pika_pubsub import Producer
...
producer = Producer.get_producer()
producer.produce(
        body={'id': order.id},
        routing_key='order.sent.order_id.1.0.0'
)"""