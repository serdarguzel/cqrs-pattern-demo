from django.db import models
import uuid


class ChainRestaurant(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(max_length=255, blank=True)

	def __str__(self):
		return self.name


class Restaurant(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(max_length=255, blank=True)
	chain_restaurant = models.ForeignKey(ChainRestaurant, on_delete=models.CASCADE, default=None, null=True, blank=True)

	def __str__(self):
		return self.name


PRODUCT_STATUS_ACTIVE = 100
PRODUCT_STATUS_INACTIVE = 200
PRODUCT_STATUS_DELETED = 300
PRODUCT_STATUS = (
	(PRODUCT_STATUS_ACTIVE, 'Active'),
	(PRODUCT_STATUS_INACTIVE, 'Inactive'),
	(PRODUCT_STATUS_DELETED, 'Deleted'),
)


class ChainProduct(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(max_length=255, blank=True)
	chain_restaurant = models.ForeignKey(ChainRestaurant, on_delete=models.CASCADE, default=None, null=True, blank=True)
	status = models.IntegerField(choices=PRODUCT_STATUS, default=PRODUCT_STATUS_ACTIVE)

	def __str__(self):
		return self.name


class Product(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, default=None, null=True, blank=True)
	chain_product = models.ForeignKey(ChainProduct, on_delete=models.CASCADE, default=None, null=True, blank=True)
	status = models.IntegerField(choices=PRODUCT_STATUS, default=PRODUCT_STATUS_ACTIVE)

	def __str__(self):
		return str(self.id)


