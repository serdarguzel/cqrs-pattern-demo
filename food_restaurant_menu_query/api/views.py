from django.http import HttpResponse
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .serializers import *
from .models import *
from faker import Faker
import pika
import json


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return


class ChainRestaurantViewSet(ModelViewSet):
    serializer_class = ChainRestaurantSerializer
    queryset = ChainRestaurant.objects.all()


class RestaurantViewSet(ModelViewSet):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()


class ChainProductViewSet(ModelViewSet):
    serializer_class = ChainProductSerializer
    queryset = ChainProduct.objects.all()


class ProductViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


# noinspection PyTypeChecker
class UpdateChainProductStatusView(UpdateAPIView):
    serializer_class = UpdateChainProductStatusSerializer
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def patch(self, request, *args, **kwargs):
        chain_product_id = self.request.data['chain_product_id']
        status = self.request.data['status']
        chain_product = ChainProduct.objects.get(pk=chain_product_id)
        products = Product.objects.filter(chain_product=chain_product)
        product_ids = []
        restaurant_ids = []
        for product in products:
            product_ids.append(str(product.id))
            restaurant_ids.append(str(product.restaurant.id))
            product.status = status
            product.save()
        chain_product.status = status
        chain_product.save()
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        channel.exchange_declare('update_chain_product_to_restaurant', durable=True)
        channel.basic_publish(exchange='food-menu-dev',
                              routing_key='foodTransaction.updateChainProductToRestaurant',
                              body=json.dumps({
                                  'productIds': product_ids,
                                  'restaurantIds': restaurant_ids,
                                  'chainProduct': {
                                      'chainProductId': str(chain_product.id),
                                      'name': chain_product.name,
                                      'status': chain_product.status,
                                      'chainRestaurant': str(chain_product.chain_restaurant.id),
                                  }
                              }))
        connection.close()
        return Response({'result': chain_product_id})


class GenerateFakeDataView(CreateAPIView):
    serializer_class = GenerateFakeDataSerializer
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request, *args, **kwargs):
        chain_restaurants = ChainRestaurant.objects.all()
        restaurants = Restaurant.objects.all()
        chain_products = ChainProduct.objects.all()
        products = Product.objects.all()
        chain_restaurants.delete()
        restaurants.delete()
        chain_products.delete()
        products.delete()
        for c_r in range(2):
            chain_restaurant = ChainRestaurant(name='Dominos Zincir ' + str(c_r + 1))
            chain_restaurant.save()

            for c_p in range(2):
                chain_product = ChainProduct(name='Pizza ' + str(c_p + 1), chain_restaurant=chain_restaurant)
                chain_product.save()

        chain_restaurants = ChainRestaurant.objects.all()
        for chain_restaurant in chain_restaurants:
            for r in range(10):
                restaurant = Restaurant(name='Dominos Şube ' + str(r + 1), chain_restaurant=chain_restaurant)
                restaurant.save()

                chain_products = ChainProduct.objects.filter(chain_restaurant=chain_restaurant)
                for chain_product in chain_products:
                    product = Product(restaurant=restaurant, chain_product=chain_product)
                    product.save()

        return Response({'result': True})
