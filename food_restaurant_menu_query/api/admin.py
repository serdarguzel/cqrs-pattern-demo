from django.contrib import admin
from .models import *


class CustomModelAdminMixin(object):

    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields]
        super(CustomModelAdminMixin, self).__init__(model, admin_site)


class ChainRestaurantAdmin(CustomModelAdminMixin, admin.ModelAdmin):
    pass


class RestaurantAdmin(CustomModelAdminMixin, admin.ModelAdmin):
    pass


class ChainProductAdmin(CustomModelAdminMixin, admin.ModelAdmin):
    pass


class ProductAdmin(CustomModelAdminMixin, admin.ModelAdmin):
    pass


admin.site.register(ChainRestaurant, ChainRestaurantAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(ChainProduct, ChainProductAdmin)
admin.site.register(Product, ProductAdmin)