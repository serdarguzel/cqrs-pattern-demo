from abc import ABC

from rest_framework import serializers
from .models import *


class ChainRestaurantSerializer(serializers.ModelSerializer):
	class Meta:
		model = ChainRestaurant
		exclude = []


class RestaurantSerializer(serializers.ModelSerializer):
	class Meta:
		model = Restaurant
		exclude = []


class ChainProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = ChainProduct
		exclude = []


class ProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		exclude = []


class UpdateChainProductStatusSerializer(serializers.Serializer):
	chain_product_id = serializers.UUIDField()
	status = serializers.IntegerField()


class GenerateFakeDataSerializer(serializers.Serializer):
	pass