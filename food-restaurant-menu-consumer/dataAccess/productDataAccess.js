const mongoose = require('mongoose');
const Product = require('../models/product');

module.exports = {
  insertProductDB: async (product) => Product.create(product),
  updateProductDB: async (productId, product) => Product.updateOne({ productId }, { $set: product }, { upsert: true }),
};
