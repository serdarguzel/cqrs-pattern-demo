const mongoose = require('mongoose');
const ChainProduct = require('../models/chainProduct');

module.exports = {
  insertChainProductDB: async (chainProduct) => ChainProduct.create(chainProduct),
  updateChainProductDB: async (chainProductId, chainProduct) => ChainProduct.updateOne({ chainProductId }, { $set: chainProduct }, { upsert: true }),
};
