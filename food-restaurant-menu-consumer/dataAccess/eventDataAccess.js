const mongoose = require('mongoose');
const Event = require('../models/event');

module.exports = {
  insertEventDB: async (event) => Event.create(event),
};
