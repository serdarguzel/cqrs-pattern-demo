const config = require('./config');
const ProductDB = require('./dataAccess/productDataAccess');
const ChainProductDB = require('./dataAccess/chainProductDataAccess');
const Event = require('./dataAccess/eventDataAccess');

const buildProductFromChain = (productIds, restaurantIds, chainProduct) => {
  return productIds.map((productId, index) => {
    const { chainProductId, chainRestaurant, ...rest } = chainProduct;
    return {
      restaurant: restaurantIds[index],
      productId,
      ...rest,
    };
  });
}

module.exports = {
  updateChainProductToRestaurantHandler: async (ch, msg, queue) => {
    const event = JSON.parse(msg.content.toString());
    const { productIds, restaurantIds, chainProduct } = event;
    const products = buildProductFromChain(productIds, restaurantIds, chainProduct);
    await Event.insertEventDB({ data: event, key: 'updateChainProductToRestaurant' });
    await ChainProductDB.updateChainProductDB(chainProduct.chainProductId, chainProduct);
    products.map(async product => {
      await ProductDB.updateProductDB(product.productId, product);
    });
    ch.ack(msg, false, true);
    console.log('updateChainProductToRestaurantHandler', msg.content.toString());
  },
};
