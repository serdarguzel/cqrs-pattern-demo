const mongoose = require('mongoose');
const { Schema } = mongoose;

const ChainProductSchema = new mongoose.Schema({
  chainProductId: { type: String, unique: true },
  name: { type: String },
  status: { type: Number },
  chainRestaurant: { type: String },
});

const ChainProduct = mongoose.model('ChainProduct', ChainProductSchema);

module.exports = ChainProduct;
