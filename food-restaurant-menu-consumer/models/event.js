const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
  data: mongoose.SchemaTypes.Mixed,
  key: String,
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;
