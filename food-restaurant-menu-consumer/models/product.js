const mongoose = require('mongoose');
const { Schema } = mongoose;

const ProductSchema = new mongoose.Schema({
  productId: { type: String, unique: true },
  name: { type: String },
  status: { type: Number },
  restaurant: { type: String },
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
