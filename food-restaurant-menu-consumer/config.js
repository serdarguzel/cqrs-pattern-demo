const config = {
  rabbitMqConnectionString: 'amqp://localhost:5672/',
  dbConnectionString: 'mongodb://localhost:27017/db',
  dbName: 'events',
  rabbitMq: {
    prefetchCount: 10,
    exchangeName: 'food-menu-dev',
    exchangeType: 'topic',
    exchangeOptions: {
      autoDelete: false,
      durable: true,
      passive: true,
    },
  },
};
module.exports = config;
