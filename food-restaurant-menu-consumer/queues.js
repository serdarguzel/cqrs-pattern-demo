const {
  updateChainProductToRestaurantHandler,
} = require('./consumerHandlers');
const config = require('./config');

const commonOptions = {
  durable: true,
  noAck: false,
};

module.exports = [
  {
    name: 'update_chain_product_to_restaurant',
    routingKey: 'foodTransaction.updateChainProductToRestaurant',
    errorQueue: {
      name: 'update_chain_product_to_restaurant_error',
      routingKey: 'foodTransaction.updateChainProductToRestaurant.error',
      options: {
        ...commonOptions,
        messageTtl: 60000,
        deadLetterExchange: config.rabbitMq.exchangeName,
        deadLetterRoutingKey: 'foodTransaction.updateChainProductToRestaurant',
      },
    },
    options: { ...commonOptions },
    consumerHandler: updateChainProductToRestaurantHandler,
  },
];
