const http = require('http');
const url = require('url');
const amqp = require('amqplib/callback_api');
const mongoose = require('mongoose');
const config = require('./config');
const queues = require('./queues');

const {
  exchangeName, exchangeOptions, exchangeType, prefetchCount,
} = config.rabbitMq;

let restaurantHexagonChangedQueueIsConnected = false;

const createQueue = (channel, queue) => {
  channel.assertQueue(queue.name, queue.options, () => {
    if (queue.consumerHandler) {
      channel.consume(queue.name, (msg) => queue.consumerHandler(channel, msg, queue));
    }
  });
  channel.bindQueue(queue.name, exchangeName, queue.routingKey);
  console.info(`${queue.name} binded succesfuly`);
};

mongoose.connect(config.dbConnectionString, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('MongoDB Connected');
  amqp.connect(config.rabbitMqConnectionString, (err, connection) => {
    connection.createChannel(async (chErr, channel) => {
      if (chErr) throw chErr;

      channel.assertExchange(exchangeName, exchangeType, exchangeOptions);

      console.log(`[*] Waiting for logs for exchange ${exchangeName}. To exit press CTRL+C`);

      channel.prefetch(prefetchCount);
      queues.forEach((queue) => {
        createQueue(channel, queue);
        if (queue.errorQueue) {
          createQueue(channel, queue.errorQueue);
        }
      });
      console.log('amqp is ready');
      restaurantHexagonChangedQueueIsConnected = true;
    });
  });
});

// Http Server for /health
const hostname = '127.0.0.1';
const port = process.env.PORT || 8080;

const server = http.createServer((req, res) => {
  const requestUrl = url.parse(req.url);
  const path = requestUrl.pathname;
  if (path === '/health') {
    console.log(new Date(Date.now()), res.statusCode, restaurantHexagonChangedQueueIsConnected);
    if (restaurantHexagonChangedQueueIsConnected) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('server is up');
    }
    else {
      res.statusCode = 202;
      res.setHeader('Content-Type', 'text/plain');
      res.end('server is not ready yet');
    }
  }
  else {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('page not found');
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
