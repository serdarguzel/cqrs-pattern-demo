# food-menu-consumer

The aim of this repository is to consume food zone messages from the food-zone-service.

## Environment Variables
Below are the environment variables used by the application, and what they mean.

- **`NAME`** the application name. Default: `name` in [`package.json`](./package.json).
- **`VERSION`** the application version. It's automatically set by deployment script as the deployment tag. Default: `version` in [`package.json`](./package.json).
- **`FOOD_TRANSACTION_DB_CONNECTION_STRING`** MongoDB connection string for `food-transaction` database.
- **`FOOD_TRANSACTION_DB_NAME`** DB name for `food-transaction` database.
- **`FOOD_TRANSACTION_RABBIT_URL`** URL for `food-transaction` RabbitMQ.
- **`MICROSERVICE_URL_RESTAURANT`** URL for `food-restaurant-service` microservice.

